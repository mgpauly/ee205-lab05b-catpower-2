###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Maxwell Pauly <mgpauly@hawaii.edu>
### @date 15_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower
all: $(TARGET)
electronVolt.o: electronVolt.cpp electronVolt.h
	$(CC) $(CFLAGS) -c electronVolt.cpp
cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp
foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp
gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp
megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp
catPower.o: catPower.cpp electronVolt.h foe.h gge.h megaton.h cat.h
	$(CC) $(CFLAGS) -c catPower.cpp
catPower: catPower.o electronVolt.o cat.o foe.o gge.o megaton.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o electronVolt.o cat.o foe.o gge.o megaton.o 
clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x | grep -q "Unknown toUnit \[x\]"
	@./catPower 20 m j | grep -q "20 m is 8.368E+16 j"
	@./catPower 20 g e | grep -q "20 g is 1.51419E+28 e"
	@./catPower 20 f m | grep -q "20 f is 4.78011E+09 m"
	@./catPower 20 c g | grep -q "20 c is 0 g"
	@./catPower 20 x f | grep -q "Unknown fromUnit \[x\]"
	@echo "All tests pass"