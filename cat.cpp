///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"
double fromCatPowerToJoule( double catPower ) {
   return catPower * 0;  // Cats do no work
}

double fromJouleToCatPower( double joule ) {
   return joule * 0;  // Cats do no work
}