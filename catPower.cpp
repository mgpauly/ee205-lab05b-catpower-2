///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: The number that we want to convert from
///    fromUnit:  The energy unit of fromValue
///    toUnit:    The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///   This program will only compile in C++ (with gpp) not in C (with gcc)
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "cat.h"
#include "electronVolt.h"
#include "foe.h"
#include "gge.h"
#include "megaton.h"
//#define DEBUG

// A Joule represents the electricity required to run a 1 W device for 1 s
const char JOULE         = 'j';

void printUsage() {
   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );
}




int main( int argc, char* argv[] ) {
   printf( "Energy converter\n" );

   if (argc > 4 || argc < 4) {
      printUsage();
   }

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit  = argv[2][0];         // Get the first character from the second argument
   toUnit    = argv[3][0];         // Get the first character from the thrid argument

#ifdef DEBUG
   printf( "fromValue = [%lG]\n", fromValue );
   printf( "fromUnit = [%c]\n", fromUnit );
   printf( "toUnit = [%c]\n", toUnit );
#endif

   double commonValue;
   switch( fromUnit ) {
      case JOULE         : commonValue = fromValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT : commonValue = fromElectronVoltsToJoule( fromValue );
                           break;
      case MEGATON       : commonValue = fromMegatonToJoule( fromValue );
                           break;
      case GGE           : commonValue = fromGgeToJoule( fromValue );
                           break;
      case FOE           : commonValue = fromFoeToJoule( fromValue );
                           break;
      case CATPOWER      : commonValue = fromCatPowerToJoule( fromValue );
                           break;
      default:
         printf( "Unknown fromUnit [%c]\n", fromUnit );
         exit( EXIT_FAILURE );
   }

   // printf( "commonValue = [%lG] joule\n", commonValue );


   double toValue;

   switch( toUnit ) {
      case JOULE         : toValue = commonValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT : toValue = fromJouleToElectronVolts( commonValue ) ;
                           break;
      case MEGATON       : toValue = fromJouleToMegaton( commonValue ) ;
                           break;
      case GGE           : toValue = fromJouleToGge( commonValue ) ;
                           break;
      case FOE           : toValue = fromJouleToFoe( commonValue ) ;
                           break;
      case CATPOWER      : toValue = fromJouleToCatPower( commonValue ) ;
                           break;
      default:
         printf( "Unknown toUnit [%c]\n", toUnit );
         exit( EXIT_FAILURE );
   }

   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );

   return ( EXIT_SUCCESS );
}