///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file electronVolt.cpp
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "electronVolt.h"

double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}


double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE ;
}
