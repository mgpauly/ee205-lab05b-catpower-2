///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Maxwell Pauly <mgpauly@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once
const char GGE           = 'g';
const double GGE_IN_A_JOULE            = 1 / 1.213e8 ;
extern double fromGgeToJoule( double gge );
extern double fromJouleToGge( double joule );